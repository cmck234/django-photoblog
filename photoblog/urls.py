from django.urls import path
from . import views

app_name = 'photoblog'

urlpatterns = [
    path('', views.PhotoListView.as_view(), name='photo_list'),
    path('<int:year>/<int:month>/<int:day>/<slug:photo>/',
         views.photo_detail,
         name='photo_detail'),
]
