from django.shortcuts import render, get_object_or_404
from .models import Photo
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView


class PhotoListView(ListView):
    queryset = Photo.published.all()
    context_object_name= 'photos'
    paginate_by = 10
    template_name = 'photoblog/photo/list.html'

def photo_detail(request, year, month, day, photo):
    photo = get_object_or_404(Photo, slug=photo, status='published',
                              publish__year=year,
                              publish__month=month,
                              publish__day=day)
    return render(request, 'photoblog/photo/detail.html', {'photo':photo})
